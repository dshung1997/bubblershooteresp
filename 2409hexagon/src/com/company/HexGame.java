package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HexGame {

    private HexGame() {
        initGame();
        createAndShowGUI();
    }

    //Thiết lập các hằng số
    final static Color COLOURBACK =  Color.WHITE;
    final static Color COLOURCELL =  Color.ORANGE;
    final static Color COLOURGRID =  Color.BLACK;
    final static Color COLOURONE = new Color(255,255,255,200);
    final static Color COLOURONETXT = Color.BLUE;
    final static Color COLOURTWO = new Color(0,0,0,200);
    final static Color COLOURTWOTXT = new Color(255,100,255);

    final static int EMPTY = 0; //Ngầm hiểu là rỗng
    final static int BSIZE = 12; //Kích thước của bảng
    final static int HEXSIZE = 60;	//Kích thước của mỗi lục giác
    final static int BORDERS = 15; //Lề

    int[][] board = new int[BSIZE][BSIZE];

    private void initGame(){

        HexMech.setSize(HEXSIZE); //Thiết lập kích thước
        HexMech.setBorders(BORDERS); //thiết lập lề

        //Khởi tạo các vị trí của lục giác là rỗng
        for (int i=0;i<BSIZE;i++) {
            for (int j=0;j<BSIZE;j++) {
                board[i][j]=EMPTY;
            }
        }

        //Cho vài giá trị vui vui :v
        board[3][3] = (int)'A';
        board[4][3] = (int)'Q';
        board[4][4] = -(int)'B';
    }

    private void createAndShowGUI()
    {
        DrawingPanel panel = new DrawingPanel();

        JFrame.setDefaultLookAndFeelDecorated(true); //Cái này cho nó đẹp thôi
        JFrame frame = new JFrame("Hex Testing 4");
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        Container content = frame.getContentPane(); //Cái đoạn này em cũng chưa hiểu lắm
        content.add(panel);

        frame.setSize( 800, 800);
        frame.setResizable(false);
        frame.setLocationRelativeTo( null );
        frame.setVisible(true);
    }

    class DrawingPanel extends JPanel
    {

        public DrawingPanel()
        {
            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
        }

        public void paintComponent(Graphics g)
        {
            Graphics2D g2 = (Graphics2D)g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            super.paintComponent(g2);

            //Vẽ khung
            for (int i=0;i<BSIZE;i++) {
                for (int j=0;j<BSIZE;j++) {
                    HexMech.drawHex(i,j,g2);
                }
            }

            //Tô màu nền
            for (int i=0;i<BSIZE;i++) {
                for (int j=0;j<BSIZE;j++) {
                    HexMech.fillHex(i,j,board[i][j],g2);
                }
            }

        }

        class MyMouseListener extends MouseAdapter {
            public void mouseClicked(MouseEvent e) {

                Point p = new Point( HexMech.pxtoHex(e.getX(),e.getY()) );
                if (p.x < 0 || p.y < 0 || p.x >= BSIZE || p.y >= BSIZE) return;

                //Xử lí khi ấn chuột
                board[p.y][p.x] += 1 ;
                if(board[p.y][p.x] > 4) board[p.y][p.x] = 0;
                board[p.y][p.x] = (int)'M';
                repaint();
            }
        }
    }


    public static void main(String[] args)
    {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                new HexGame();
            }
        });
    }

}
