package com.company;

import java.awt.*;


public class HexMech
{

    //Mọi người xem ảnh này hex ở bên trái: res/hex.png để hiểu cái s, a, r, h nhé !

    //Constant
    private static int BORDERS=50;	//Lề

    private static int s=0;
    private static int a=0;
    private static int r=0;
    private static int h=0;

    public static void setBorders(int b){
        BORDERS=b;
    } //Thiết lập thông số cho lề


    //Khởi tạo các giá trị của a, r, s, h dựa trên giá trị của 1 cạnh
    public static void setSize(int size) {
        a = size;
        r = a/2;
        s = (int) (a / 1.73205);
        h = s/2;
    }

    //Thiết lập các tọa độ cho 1 lục giác
    public static Polygon hex (int x0, int y0) {

        int y = y0 + BORDERS;
        int x = x0 + BORDERS;

        if (s == 0  || h == 0) {
            System.out.println("ERROR: size of hex has not been set");
            return new Polygon();
        }

        int[] cx,cy;
        cx = new int[] {x,  x+r,    x+r,    x,          x-r,    x-r};
        cy = new int[] {y,  y+h,    y+h+s,  y+h+s+h,    y+h+s,  y+h};

        return new Polygon(cx,cy,6);
    }

    //Vẽ khung cho lục giác [i, j]
    public static void drawHex(int i, int j, Graphics2D g2) {

//        BufferedImage bubble = null;
//        try {
//            bubble = ImageIO.read(new File("res/bubble1.png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        int x = (j+1)*a - ((i+1)%2)*r;
        int y = i*(s+h);

        Polygon poly = hex(x,y);
        g2.setColor(HexGame.COLOURCELL);
        g2.fillPolygon(poly);
        g2.setColor(HexGame.COLOURGRID);
        g2.drawPolygon(poly);

        //g2.drawImage(bubble, x+ BORDERS+6, y+ BORDERS+3, null);

    }

    //Tô màu cho lục giác [i, j]
    public static void fillHex(int i, int j, int n, Graphics2D g2) {
        char c='o';

        int x = (j+1)*a - ((i+1)%2)*r;
        int y = i*(s+h);

        if (n < 0) {
            g2.setColor(HexGame.COLOURONE);
            g2.fillPolygon(hex(x,y));
            g2.setColor(HexGame.COLOURONETXT);
            c = (char)(-n);
            g2.drawString(""+c, x+BORDERS, y+s+BORDERS);

        }
        if (n > 0) {
            g2.setColor(HexGame.COLOURTWO);
            g2.fillPolygon(hex(x,y));
            g2.setColor(HexGame.COLOURTWOTXT);
            c = (char)n;
            g2.drawString(""+c, x+BORDERS, y+s+BORDERS);
        }
    }

    //Xác định 1 điểm trên màn hình thuộc vào lục giác nào
    public static Point pxtoHex(int mx, int my) {
        Point p = new Point(-1,-1);

        //Bỏ lề
        mx -= BORDERS;
        my -= BORDERS;

        //Tạm thời lấy giá trị của x, y. [x, y] là tọa độ trong mảng 2 chiều
        int y = (int) (my / (s+h));
        int x = (int) (mx / a);

        //dx, dy là khoảng cách từ điểm đó tới trục x và trục y gần nhất
        int dx = mx - x*a;
        int dy = my - y*(s+h);

        //Đoạn này hôm nay đi học e sẽ giải thích rõ hơn
        if (x % 2 == 0) {

            if(y % 2 == 0){

                if (dx > r) {
                    if ( (2*r-dx)*h + dy*r < h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dx*h + dy*r < h*r ){
                        y--;
                        x--;
                    }
                }
            }

            else {

                if (dx > r) {
                    if ( dx*h + dy*r < 2*h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dy*r < dx*h ){
                        y--;
                    }
                    else if( dy*r > dx*h ){
                        x--;
                    }
                }

            }

        } else {
            if(y % 2 == 0){

                if (dx > r) {
                    if ( dy*r < h*dx - h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dx*h + dy*r < h*r ){
                        y--;
                        x--;
                    }
                }
            }

            else {

                if (dx > r) {
                    if ( dx*h + dy*r < 2*h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dy*r < dx*h ){
                        y--;
                    }
                    else if( dy*r > dx*h ){
                        x--;
                    }
                }

            }

        }

        p.x=x;
        p.y=y;
        return p;
    }
}