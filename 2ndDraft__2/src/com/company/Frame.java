package com.company;

import com.sun.prism.Texture;
import javafx.scene.image.*;
import javafx.scene.image.Image;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Created by dshunggggg on 16/09/2016.
 */
public class Frame extends JFrame{

//    BufferedImage background = ImageIO.read(new File("res/background.png"));
//
//    BufferedImage newgameBtn = ImageIO.read(new File("res/newgameBtn.png"));
//
//    BufferedImage highscoreBtn = ImageIO.read(new File("res/highscoreBtn.png"));
//
//    BufferedImage aboutBtn1 = ImageIO.read(new File("res/about1Btn.png"));
//
//    BufferedImage bubble = ImageIO.read(new File("res/bubble1.png"));

    public Frame(int width, int height, String title, Game game) throws IOException {


        JFrame frame = new JFrame(title);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setPreferredSize(new Dimension(width, height));
        frame.setMaximumSize(new Dimension(width, height));
        frame.setMinimumSize(new Dimension(width, height));

        frame.setResizable(false);

        frame.setLocationRelativeTo(null);



        frame.add(game);

//        JLabel bg = new JLabel(new ImageIcon(background));
//        bg.add(createJButton(150, 500, newgameBtn));
//        bg.add(createJButton(150, 400, highscoreBtn));
//        bg.add(createJButton(420, 640, aboutBtn1));
//       bg.add(createJLabel(190, 591, bubble));
//        frame.add(bg);

        frame.setVisible(true);
        game.start();

    }

    public JLabel createJLabel(int x, int y, BufferedImage img){
        JLabel lab = new JLabel(new ImageIcon(img));
        lab.setLocation(x, y);
        lab.setSize(100, 100);
        return lab;
    }

    public JButton createJButton(int x, int y, BufferedImage img){
        JButton ng = new JButton(new ImageIcon(img));
        ng.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("New game");
            }
        });
        ng.setLocation(x, y);
        ng.setSize(img.getWidth(), img.getHeight());
//        Border emptyBorder = BorderFactory.createEmptyBorder();
//        ng.setBorder(emptyBorder);
        return ng;
    }

}
