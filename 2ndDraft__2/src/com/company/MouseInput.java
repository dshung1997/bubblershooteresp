package com.company;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by dshunggggg on 23/09/2016.
 */
public class MouseInput implements MouseMotionListener, MouseListener {
    private Handler handler;

    public MouseInput(Handler handler){
        this.handler = handler;
    }



    @Override
    public void mouseClicked(MouseEvent e) {
        double eX = e.getX()-26;
        double eY = e.getY()-26;
        int k = 20;
        for(int i = 0; i < handler.object.size(); i++){
            BubbleObject tempObject = handler.object.get(i);

            double disX = Math.abs(eX - tempObject.getX());
            double disY = Math.abs(eY - tempObject.getY());
            double dis = Math.sqrt(Math.pow(disX, 2) + Math.pow(disY, 2));

            if(tempObject.getX() >= eX) tempObject.setVelX((int) (-k * disX / dis));
            else tempObject.setVelX((int) (+k * disX / dis));

            if(tempObject.getY() >= eY) tempObject.setVelY((int) (-k * disY / dis));
            else tempObject.setVelY((int) (+k * disY / dis));


        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
