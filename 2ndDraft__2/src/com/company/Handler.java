package com.company;


import java.awt.*;
import java.util.LinkedList;

/**
 * Created by dshunggggg on 09/09/2016.
 */
public class Handler {
    LinkedList<BubbleObject> object = new LinkedList<BubbleObject>();

    public void tick(){
        for(int i = 0; i <object.size(); i++){
            BubbleObject tempObject = object.get(i);
            tempObject.tick();
        }

    }

    public void render(Graphics g){
        for(int i = 0; i < object.size(); i++){
            BubbleObject tempObject = object.get(i);

            tempObject.render(g);
        }
    }

    public void addObject(BubbleObject object){
        this.object.add(object);
    }

    public void removeObject(BubbleObject object){
        this.object.remove(object);
    }
}
