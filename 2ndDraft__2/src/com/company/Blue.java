package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by dshunggggg on 23/09/2016.
 */
public class Blue extends BubbleObject {

    BufferedImage bubble1 = ImageIO.read(new File("res/bubble2.png"));
    float a;

    public Blue(int x, int y, ID id) throws IOException {
        super(x, y, id);
        velX = 0;
        velY = 0;
        img = bubble1;
    }

    @Override
    public void tick() {
        x += velX;
        y += velY;

        if (x <= 0 || x >= Game.WIDTH - 60) velX *= -1;
        if (y >= (Game.HEIGHT - 83)) velY *= -1;
        if(y <= 1) {velX = 0; velY = 0;}


    }

    @Override
    public void render(Graphics g) {
        g.drawImage(img, x, y, null);
    }

    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
}
