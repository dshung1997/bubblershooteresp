package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Game extends Canvas implements Runnable {

    public static final int WIDTH = 480, HEIGHT = 720;
    private Thread thread;
    private boolean running = false;
    private Handler handler;

    public Game() throws IOException {

        handler = new Handler();

        this.addMouseListener(new MouseInput(handler));

        new Frame(WIDTH, HEIGHT, "Dynamite !", this);
        handler.addObject(new Blue(212, 591, ID.B1));
//        handler.addObject(new Blue(150, 591, ID.B1));

    }

    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }
    public synchronized void stop(){
        try{
            thread.join();
            running = false;
        } catch(InterruptedException e){
            e.printStackTrace();
        }

    }

    @Override
    public void run() {

        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;

        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            while(delta >= 1){
                tick();
                delta--;
            }

            if(running){
                try {
                    render();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                System.out.println("FPS: "+ frames);
                frames = 0;
            }
        }
        stop();
    }

    public void update(){

    }

    public void tick(){
        handler.tick();
    }

    private void render() throws IOException {
        BufferStrategy bs = this.getBufferStrategy();
        if(bs == null){
            this.createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();

        BufferedImage background = ImageIO.read(new File("res/background.png"));
        g.drawImage(background, 0, 0, this);
//        g.setColor(new Color(0xAE8BCD));
//        g.fillRect(0, 0, WIDTH, HEIGHT);

        handler.render(g);

        g.dispose();
        bs.show();

    }

    public static void main(String[] args) throws IOException {
        new Game();
    }

}
