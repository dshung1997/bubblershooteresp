package com.company;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Created by dshunggggg on 16/09/2016.
 */
public class Frame extends JFrame{
    BufferedImage newgameBtn = ImageIO.read(new File("res/newgameBtn.png"));

    public Frame(int width, int height, String title, BubbleShooter bb) throws IOException {

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame(title);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setPreferredSize(new Dimension(width, height));
        frame.setMaximumSize(new Dimension(width, height));
        frame.setMinimumSize(new Dimension(width, height));

        frame.setResizable(false);

        frame.setLocationRelativeTo(null);
        frame.add(createJButton(150, 650, newgameBtn));

        frame.add(bb);



        frame.setVisible(true);
        bb.start();





    }

//    public JLabel createJLabel(int x, int y, BufferedImage img){
//        JLabel lab = new JLabel(new ImageIcon(img));
//        lab.setLocation(x, y);
//        lab.setSize(100, 100);
//        return lab;
//    }
//
    public JButton createJButton(int x, int y, BufferedImage img){
        JButton ng = new JButton(new ImageIcon(img));
        ng.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        ng.setLocation(x, y);
        ng.setSize(img.getWidth(), img.getHeight());
//        Border emptyBorder = BorderFactory.createEmptyBorder();
//        ng.setBorder(emptyBorder);
        return ng;
    }

}
