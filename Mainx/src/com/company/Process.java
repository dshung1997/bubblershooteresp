package com.company;

import java.awt.*;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by dshunggggg on 05/10/2016.
 */
public class Process {
    public static int numberCollision = 0;
    public static int[][] bubbleColor;
    static Random random = new Random();

    public static void findSameColor(Handler handler){
        bubbleColor = new int[6][100];

        for(int i = 1; i < 6; i++){
            Arrays.fill(bubbleColor[i], -1);
        }


        int i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0;
        for(int i = 0; i < handler.listBubble.size(); i++){
            GameObject tempGameObject = handler.listBubble.get(i);
            int c = tempGameObject.color;
            switch (c){
                case 1 : {
                    if(!contains(bubbleColor[1], i)) {
                        bubbleColor[1][i1] = i;
                        i1++;
                    }
                    break;
                }

                case 2 : {
                    if(!contains(bubbleColor[2], i)) {
                        bubbleColor[2][i2] = i;
                        i2++;
                    }
                    break;
                }

                case 3 : {
                    if(!contains(bubbleColor[3], i)) {
                        bubbleColor[3][i3] = i;
                        i3++;
                    }
                    break;
                }

                case 4 : {
                    if(!contains(bubbleColor[4], i)) {
                        bubbleColor[4][i4] = i;
                        i4++;
                    }
                    break;
                }

                case 5 : {
                    if(!contains(bubbleColor[5], i)) {
                        bubbleColor[5][i5] = i;
                        i5++;
                    }
                    break;
                }
            }
        }

        for(int i = 1; i < 6; i++){
            bubbleColor[i] = sortArray(bubbleColor[i]);
        }
    }

    public static void findClusters(Handler handler, int index, int color){
        int[] willBeRemoved = new int[100];
        int iWillBeRemoved = 0;
        Arrays.fill(willBeRemoved, -1);

        GameObject mainObject = handler.listBubble.get(index);
        for(int i = 0; i < bubbleColor[color].length; i++){
            if(bubbleColor[color][i] == -1) break;
            GameObject tempObject = handler.listBubble.get(bubbleColor[color][i]);
            if(Hex.createRect(tempObject.vertex).getBounds2D().intersects(Hex.createRect(mainObject.vertex).getBounds2D())){
                if(!contains(willBeRemoved, bubbleColor[color][i])){
                    willBeRemoved[iWillBeRemoved] = bubbleColor[color][i];
                    iWillBeRemoved++;
                }
            }
        }


        int j = 0;
        do{
            GameObject mObject = handler.listBubble.get(willBeRemoved[j]);
            for(int i = 0; i < bubbleColor[color].length; i++){
                if(bubbleColor[color][i] == -1) break;
                GameObject tObject = handler.listBubble.get(bubbleColor[color][i]);
                if(Hex.createRect(mObject.vertex).getBounds().intersects(Hex.createRect(tObject.vertex).getBounds())){
                    if(!contains(willBeRemoved, bubbleColor[color][i])){
                        willBeRemoved[iWillBeRemoved] = bubbleColor[color][i];
                        iWillBeRemoved++;
                    }
                }
            }
            j++;
        }while(willBeRemoved[j] != -1);

        if(iWillBeRemoved >= 3){
            numberCollision += 2;
            willBeRemoved = sortArray(willBeRemoved);
            for(int i = 0; i < iWillBeRemoved; i++){
                GameObject temp = handler.listBubble.get(willBeRemoved[i]);
                temp.isRemoved = true;
                handler.removeObject(temp);
            }
        }

        HUD.isFlying = false;

    }

    public static int[] sortArray(int[] arr){
        for(int k = 0; k < arr.length-1; k++){
            for(int l = k+1; l < arr.length; l++){
                if(arr[l] > arr[k]){
                    int temp = arr[l];
                    arr[l] = arr[k];
                    arr[k] = temp;
                }
            }
        }

        return arr;
    }

    public static boolean contains(int[] array, int m) {
        boolean contains = IntStream.of(array).anyMatch(x -> x == m);
        return contains;
    }

    public static BubbleInGrid addOneBubble(Handler handler, Point p){
        int color = random.nextInt(5)+1;
        BubbleInGrid nb = new BubbleInGrid(p, color, ID.Normal);
        return nb;
    }


    public static void addOneRow(Handler handler){
        Point p = Hex.lastBubble;

        if(Hex.lastBubble.x == (Hex.bordersX + Hex.r)){
            p.x += Hex.r;
        }
        else p.x -= Hex.r;
        p.y -= (Hex.s + Hex.h);
        for(int i = 0; i < 6; i++){
            handler.addObject(addOneBubble(handler, new Point(p.x + i*(Hex.a), p.y)));
        }

        Hex.lastBubble = new Point(p.x , p.y);
    }



}
