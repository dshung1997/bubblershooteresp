package com.company;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by dshunggggg on 23/09/2016.
 */
public class MouseInput implements MouseMotionListener, MouseListener {
    private Handler handler;
    private HUD hud;


    public MouseInput(Handler handler, HUD hud){
        this.hud = hud;
        this.handler = handler;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println(e.getPoint());
        if(!HUD.isFlying){
            Point p = e.getPoint();
            double k = 20.0;
            for(int i = 0; i < handler.listBubble.size(); i++){
                GameObject tempObject = handler.listBubble.get(i);
                if(tempObject.id == ID.Player){

                    double disX = Math.abs(p.x - tempObject.center.x);
                    double disY = Math.abs(p.y - tempObject.center.y);
                    double dis = Math.sqrt(Math.pow(disX, 2) + Math.pow(disY, 2));

                    if (dis > 4) {
                        if (tempObject.center.x >= p.x) {
                            tempObject.velX = ((int) (-k * disX / dis));
                        }
                        else {
                            tempObject.velX = ((int) (+k * disX / dis));
                        }

                        if (tempObject.center.y >= p.y) {
                            tempObject.velY = ((int) (-k * disY / dis));
                        }
                        else {
                            tempObject.velY = ((int) (+k * disY / dis));
                        }
                    } else {
                        tempObject.velX = 0;
                        tempObject.velY = 0;
                    }
                    break;
                }

            }

            hud.newBubble();
            HUD.isFlying = true;
            Process.findSameColor(handler);
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
