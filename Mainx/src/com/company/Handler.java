package com.company;

import java.awt.*;
import java.util.LinkedList;

/**
 * Created by dshunggggg on 29/09/2016.
 */
public class Handler {
    LinkedList<GameObject> listBubble = new LinkedList<GameObject>();

    public void update(){
        for(int i = 0; i < listBubble.size(); i++){
            GameObject tempGameObject = listBubble.get(i);
            tempGameObject.update();
        }
    }


    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        for(int i = 0; i < listBubble.size(); i++){
            GameObject tempGameObject = listBubble.get(i);
            tempGameObject.render(g2d);
        }
    }

    public void addObject(GameObject newObject){
        this.listBubble.add(newObject);
    }

    public void removeObject(GameObject oldObject){
        this.listBubble.remove(oldObject);
    }

}
