package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class HexAlgo
{

    //Mọi người xem ảnh này hex ở bên trái: res/hex.png để hiểu cái s, a, r, h nhé !

    //Constant
    private static int BORDERS=15;	//Lề

    public static int s=0;
    public static int a=0;
    public static int r=0;
    public static int h=0;

    //Khởi tạo các giá trị của a, r, s, h dựa trên giá trị của 1 cạnh
    public static void setSize(int size) {
        a = size;
        r = a/2;
        s = (int) (a / 1.73205);
        h = s/2;
    }


    public static Polygon hex (Point p) {
        int x = p.x;
        int y = p.y;

        int[] cx,cy;
        cx = new int[] {x,  x+r,    x+r,    x,          x-r,    x-r};
        cy = new int[] {y,  y+h,    y+h+s,  y+h+s+h,    y+h+s,  y+h};

        return new Polygon(cx,cy,6);
    }

    //Vẽ khung cho lục giác [i, j]
    public static void drawHex(Point p, int id, Graphics2D g2) {

        BufferedImage bb = null;
        try {
            bb = ImageIO.read(new File("res/bu"+id+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        int rc = (int) (s * 0.866025403);

        g2.drawImage(bb, p.x - rc , p.y + (s-rc), null);
        //g2.drawImage(bb, p.x , p.y , null);
        Polygon poly = hex(p);
        g2.drawPolygon(poly);

    }

    //Xác định 1 điểm trên màn hình thuộc vào lục giác nào
    public static Point getGridPosition(int mx, int my) {
        Point p = new Point(-1,-1);

        //Bỏ lề
        mx -= BORDERS;
        my -= BORDERS;

        //Tạm thời lấy giá trị của x, y. [x, y] là tọa độ trong mảng 2 chiều
        int y = (int) (my / (s+h));
        int x = (int) (mx / a);

        //dx, dy là khoảng cách từ điểm đó tới trục x và trục y gần nhất
        int dx = mx - x*a;
        int dy = my - y*(s+h);

        //Đoạn này hôm nay đi học e sẽ giải thích rõ hơn
        if (x % 2 == 0) {

            if(y % 2 == 0){

                if (dx > r) {
                    if ( (2*r-dx)*h + dy*r < h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dx*h + dy*r < h*r ){
                        y--;
                        x--;
                    }
                }
            }

            else {

                if (dx > r) {
                    if ( dx*h + dy*r < 2*h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dy*r < dx*h ){
                        y--;
                    }
                    else if( dy*r > dx*h ){
                        x--;
                    }
                }

            }

        } else {
            if(y % 2 == 0){

                if (dx > r) {
                    if ( dy*r < h*dx - h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dx*h + dy*r < h*r ){
                        y--;
                        x--;
                    }
                }
            }

            else {

                if (dx > r) {
                    if ( dx*h + dy*r < 2*h*r ) {
                        y--;
                    }
                }
                if (dx < r) {
                    if( dy*r < dx*h ){
                        y--;
                    }
                    else if( dy*r > dx*h ){
                        x--;
                    }
                }

            }

        }

        p.x=x;
        p.y=y;
        return p;
    }

    public static Point getVertexFromGrid(int i, int j){
        return new Point((j+1)*a - ((i+1)%2)*r + BORDERS, i*(s+h) + BORDERS);
    }

    public static Point getCenterFromVertex(Point p){
        return new Point(p.x, p.y+s);
    }

    public static Point getVertexFromCenter(Point p){
        return new Point(p.x, p.y-s);
    }
}