package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class HexGame {

    private HexGame() {
        initGame();
        createAndShowGUI();
    }

    final static int BSIZE = 6; //Kích thước của bảng
    final static int HEXSIZE = 60;	//Kích thước của mỗi lục giác
    //static boolean move = false;
    static boolean isMove = false;


    int[][] normal = new int[BSIZE][BSIZE]; //Vi tri cua luc giac
    public static Polygon[][] normalPolygon  = new Polygon[BSIZE][BSIZE];
    public static Point[][] normalVertex = new Point[BSIZE][BSIZE];
    public static Point[][] normalCenter = new Point[BSIZE][BSIZE];

    int player = 0;
    public static Polygon playerPolygon  = new Polygon();
    public static Point playerVertex  = new Point();
    public static Point playerCenter  = new Point();

    public static int velX = 3, velY = 3;

    private void initGame(){

        HexAlgo.setSize(HEXSIZE); //Thiết lập kích thước

        //Khởi tạo các vị trí của lục giác random
        Random r = new Random();
        for (int i=0;i<BSIZE;i++) {
            for (int j=0;j<BSIZE;j++) {
                int id = r.nextInt(4) + 1;
                normal[i][j] = id;
                normalVertex[i][j] = HexAlgo.getVertexFromGrid(i, j);
                normalCenter[i][j] = HexAlgo.getCenterFromVertex(normalVertex[i][j]);
                normalPolygon[i][j] = HexAlgo.hex(normalVertex[i][j]);
            }
        }

        player =  2;
        playerVertex = new Point(230, 600);
        playerCenter = HexAlgo.getCenterFromVertex(playerVertex);
        playerPolygon = HexAlgo.hex(playerVertex);

    }

    private void createAndShowGUI()
    {

        DrawingPanel panel = new DrawingPanel();

        JFrame.setDefaultLookAndFeelDecorated(true); //Cái này cho nó đẹp thôi
        JFrame frame = new JFrame("Hex Testing 4");
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        Container content = frame.getContentPane(); //Cái đoạn này em cũng chưa hiểu lắm
        content.add(panel);

        frame.setSize(450, 720);
        frame.setResizable(false);
        frame.setLocationRelativeTo( null );
        frame.setVisible(true);
    }


    class DrawingPanel extends JPanel
    {

        public DrawingPanel()
        {
            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
        }

        @Override
        public void paintComponent(Graphics g)
        {
            BufferedImage background = null;
            try {
                background = ImageIO.read(new File("res/background.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Graphics2D g2 = (Graphics2D)g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            super.paintComponent(g2);
            g2.drawImage(background, 0, 0, null);

            for (int i=0;i<BSIZE;i++) {
                for (int j=0;j<BSIZE;j++) {
                    HexAlgo.drawHex(normalVertex[i][j], normal[i][j], g2);
                }
            }

            HexAlgo.drawHex(playerVertex,  player, g2);

        }

        class MyMouseListener extends MouseAdapter implements Runnable{

            public void moveBubble(){
                playerCenter.x += velX;
                playerCenter.y += velY;
                if(playerCenter.x < 0 || playerCenter.x > 570) velX *= -1;
                if(playerCenter.y < 0 || playerCenter.y > 770) velY *= -1;

                playerVertex = HexAlgo.getVertexFromCenter(playerCenter);
                repaint();
            }
            public void mouseClicked(MouseEvent e) {
                playerCenter = e.getPoint();
                playerVertex = HexAlgo.getVertexFromCenter(playerCenter);
                repaint();
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
                mouseReleased(e);
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
            }

            public void mouseMoved(MouseEvent e) {
            }

            @Override
            public void run() {
                if(isMove) {
                    moveBubble();
                }
            }
        }
    }



    public static void main(String[] args)
    {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                new HexGame();

            }
        });
    }

}
