package com.company;

import java.awt.*;

/**
 * Created by dshunggggg on 01/10/2016.
 */
public class Player extends GameObject {

    private int s = Hex.s;
    private int a = Hex.a;
    private int borX = Hex.bordersX;

    private Handler handler;
    private HUD hud;
    private boolean isCollided;


    public Player(Point p, int color, ID id, Handler handler, HUD hud){
        super(p, color, id);
        this.handler = handler;
        this.hud = hud;
        velX = 0;
        velY = 0;
        isCollided = false;
    }

    public void update(){

        center.y += velY;
        center.x += velX;

        if (center.x <= Hex.bordersX + 28 || center.x >= BubbleShooter.WIDTH - a/2-20) velX *= -1;
        if (center.y <= s || center.y >= (BubbleShooter.HEIGHT - s - 34)) velY *= -1;

        vertex = Hex.getVertexFromCenter(center);

        if(HUD.isFlying) checkCollision();
//        if(isCollided) afterCollision();
    }

    @Override
    public void render(Graphics2D g2d){

        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        Hex.drawHex(vertex, color, g2d);

        if(isRemoved){
            g2d.dispose();
        }
    }

    public void checkCollision() {
        for (int i = 0; i < handler.listBubble.size(); i++) {
            GameObject tempObject = handler.listBubble.get(i);
            if (tempObject.id == ID.Normal) {
                if(checkCollision(center, tempObject.center)){
                    isCollided = true;
                    handler.removeObject(this);
                    afterCollision();
                    return;
                }
            }

        }
    }

    public void afterCollision(){
        hud.snapBubble(center, color);
    }

    public boolean checkCollision(Point p1, Point p2){
        double a = 0.0;
        a = Math.sqrt(Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y), 2));
        if(a < 50) return true;
        else return false;
    }

}
