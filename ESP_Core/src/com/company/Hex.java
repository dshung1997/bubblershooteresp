package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class Hex
{
    public static Point lastBubble;
    public static boolean isEven = true;
    public static int curY = 0;

    public static int bordersX=0;

    public static int s=0;
    public static int a=0;
    public static int r=0;
    public static int h=0;
    public static int rc=0;

    public static void setBORDERS(int X){
        bordersX = X;
        curY = 0;
    }

    public static void setSize(int size) {
        a = size;
        r = a/2;
        s = (int) (a / 1.73205);
        h = s/2;
        rc = (int) (s * 0.866025403);
        lastBubble = new Point(bordersX + r,0);
    }

    public static Rectangle createRect (Point p){
        int x = p.x - r - 2 ;
        int y = p.y - 2;
        return new Rectangle(x, y, a + 4, 2*s + 4);
    }

    public static Polygon creatHex (Point p) {
        int x = p.x;
        int y = p.y;

        int[] cx,cy;
        cx = new int[] {x,  x+r,    x+r,    x,          x-r,    x-r};
        cy = new int[] {y,  y+h,    y+h+s,  y+h+s+h,    y+h+s,  y+h};

        return new Polygon(cx,cy,6);
    }

    public static void drawHex(Point p, int color, Graphics2D g2d) {

        BufferedImage bb = null;
        try {
            bb = ImageIO.read(new File("res/bu"+color+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Polygon poly = creatHex(p);
        g2d.drawPolygon(poly);
        g2d.drawImage(bb, p.x - rc , p.y + (s-rc)-1 , null);
    }

    public static Point getGridPositionOdd(Point p) {

        int mx = (int) p.getX();
        int my = (int) p.getY();

        mx -= bordersX;
        my -= curY;

        int y = (int) (my / (s+h));
        int x = (int) (mx / a);

        int dx = mx - x*a;
        int dy = my - y*(s+h);

        if (y % 2 == 0) {

            if (dx > r) {
                if (dx * h + dy * r < 2 * h * r) {
                    y--;
                }
            }
            if (dx < r) {
                if (dy * r < dx * h) {
                    y--;
                } else if (dy * r > dx * h) {
                    x--;
                }
            }

        } else {

            if (dx > r) {
                if (dy * r < h * dx - h * r) {
                    y--;
                }
            }
            if (dx < r) {
                if (dx * h + dy * r < h * r) {
                    y--;
                    x--;
                }
            }

        }

        return new Point(y, x);
    }

    public static Point getGridPositionEven(Point p) {

        int mx = (int) p.getX();
        int my = (int) p.getY();

        mx -= bordersX;
        my -= curY;

        int y = (int) (my / (s+h));
        int x = (int) (mx / a);

        int dx = mx - x*a;
        int dy = my - y*(s+h);

        if (y % 2 == 0) {

            if (dx > r) {
                if (dy * r < h * dx - h * r) {
                    y--;
                }
            }
            if (dx < r) {
                if (dx * h + dy * r < h * r) {
                    y--;
                    x--;
                }
            }
        } else {

            if (dx > r) {
                if (dx * h + dy * r < 2 * h * r) {
                    y--;
                }
            }
            if (dx < r) {
                if (dy * r < dx * h) {
                    y--;
                } else if (dy * r > dx * h) {
                    x--;
                }
            }

        }

        return new Point(y, x);
    }

    public static Point getGridPosition(Point p){
        if(isEven) {
            return getGridPositionEven(p);
        }
        else {
            return getGridPositionOdd(p);
        }
    }

    public static Point getVertexFromGrid(boolean isInGrid, int i, int j){
        if(isInGrid){
            return new Point((j+1)*a - ((i+1)%2)*r + bordersX, -i*(s+h));
        }
        else {
            if(isEven) return new Point((j+1)*a - ((i+1)%2)*r + bordersX, +i*(s+h) + curY);
            else return new Point((j+1)*a - ((i)%2)*r + bordersX, +i*(s+h) + curY);
        }

    }

    public static Point getCenterFromVertex(Point p){
        return new Point(p.x, p.y+s);
    }

    public static Point getVertexFromCenter(Point p){
        return new Point(p.x, p.y-s);
    }

    public static void viewCurXCurY(){
        lastBubble.y++;
        curY++;
        if(curY >= (s+h)){
            curY = curY % (s+h) ;
            if(isEven) isEven = false;
            else isEven = true;
        }
    }

}