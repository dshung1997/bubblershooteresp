package com.company;

import java.awt.*;
import java.awt.geom.Area;
import java.util.Random;

/**
 * Created by dshunggggg on 30/09/2016.
 */
public class HUD {
    public static boolean isFlying;
    public int allBubble = 0;

    Random rd = new Random();
    private Handler handler;
    public Player player;

    public HUD (Handler handler){
        this.handler = handler;
        isFlying = false;
    }

    public void newBubble(){
        int color = rd.nextInt(5)+1;
        Point np = new Point(240, 600);
        player = new Player(np, color, ID.Player, handler, this);
        handler.addObject(player);
//        Process.findSameColor(handler);
    }

    public void snapBubble(Point p, int color){
        Point pGrid = Hex.getGridPosition(p);
        Point pVertex = Hex.getVertexFromGrid(false, pGrid.x, pGrid.y);
        BubbleInGrid a = new BubbleInGrid(pVertex, color, ID.Normal);
        handler.addObject(a);
        int index = handler.listBubble.indexOf(a);
        Process.findSameColor(handler);
        Process.findClusters(handler, index, color);
//        return index;
    }

}


