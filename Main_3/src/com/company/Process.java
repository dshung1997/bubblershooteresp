package com.company;

import java.awt.*;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by dshunggggg on 05/10/2016.
 */
public class Process {

    public static int indexPlayer;

    public static int[] solidBubble;
    public static int iSolid;

    public static int[] willBeRemoved;
    public static int iWillBeRemoved;

    public static int numberCollision = 0;
    public static int[][] bubbleColor;
    static Random random = new Random();

    public static void findSameColor(Handler handler){
        System.out.println(handler.listBubble.size());
        bubbleColor = new int[6][100];

        for(int i = 1; i < 6; i++){
            Arrays.fill(bubbleColor[i], -1);
        }


        iSolid = 0;
        solidBubble = new int[100];
        Arrays.fill(solidBubble, -1);


        int i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0;
        for(int i = 0; i < handler.listBubble.size(); i++){
            GameObject tempGameObject = handler.listBubble.get(i);

            //Set the value of being attached to the grid be FALSE;
            tempGameObject.isSolid = false;

            if(Math.abs(tempGameObject.vertex.y - Hex.lastBubble.y ) <= 2){
                if(!contains(solidBubble, i)){
                    solidBubble[iSolid] = i;
                    iSolid++;
                    tempGameObject.isSolid = true;
                }
            }

            int c = tempGameObject.color;
            switch (c){
                case 1 : {
                    if(!contains(bubbleColor[1], i)) {
                        bubbleColor[1][i1] = i;
                        i1++;
                    }
                    break;
                }

                case 2 : {
                    if(!contains(bubbleColor[2], i)) {
                        bubbleColor[2][i2] = i;
                        i2++;
                    }
                    break;
                }

                case 3 : {
                    if(!contains(bubbleColor[3], i)) {
                        bubbleColor[3][i3] = i;
                        i3++;
                    }
                    break;
                }

                case 4 : {
                    if(!contains(bubbleColor[4], i)) {
                        bubbleColor[4][i4] = i;
                        i4++;
                    }
                    break;
                }

                case 5 : {
                    if(!contains(bubbleColor[5], i)) {
                        bubbleColor[5][i5] = i;
                        i5++;
                    }
                    break;
                }
            }

        }

        for(int i = 1; i < 6; i++){
            bubbleColor[i] = sortArray(bubbleColor[i]);
        }
        solidBubble = sortArray(solidBubble);
    }

    public static void findClusters(Handler handler, int index, int color){
        willBeRemoved = new int[100];
        iWillBeRemoved = 0;

        Arrays.fill(willBeRemoved, -1);

        GameObject mainObject = handler.listBubble.get(index);
        for(int i = 0; i < bubbleColor[color].length; i++){
            if(bubbleColor[color][i] == -1) break;
            GameObject tempObject = handler.listBubble.get(bubbleColor[color][i]);
            if(Hex.createRect(tempObject.vertex).getBounds2D().intersects(Hex.createRect(mainObject.vertex).getBounds2D())){
                if(!contains(willBeRemoved, bubbleColor[color][i])){
                    willBeRemoved[iWillBeRemoved] = bubbleColor[color][i];
                    iWillBeRemoved++;
                }
            }
        }


        int j = 0;
        do{
            GameObject mObject = handler.listBubble.get(willBeRemoved[j]);
            for(int i = 0; i < bubbleColor[color].length; i++){
                if(bubbleColor[color][i] == -1) break;
                GameObject tObject = handler.listBubble.get(bubbleColor[color][i]);
                if(Hex.createRect(mObject.vertex).getBounds().intersects(Hex.createRect(tObject.vertex).getBounds())){
                    if(!contains(willBeRemoved, bubbleColor[color][i])){
                        willBeRemoved[iWillBeRemoved] = bubbleColor[color][i];
                        iWillBeRemoved++;
                    }
                }
            }
            j++;
        }while(willBeRemoved[j] != -1);

        if(iWillBeRemoved >= 3){
            numberCollision += 2;

            findFloatingBubbles(handler);

            for(int i = 0; i < handler.listBubble.size(); i++){
                if(!contains(solidBubble, i) && !contains(willBeRemoved, i)){
                    willBeRemoved[iWillBeRemoved] = i;
                    iWillBeRemoved++;
                }
            }

            for(int i = 0; i < iWillBeRemoved; i++){
                if(willBeRemoved[i] == indexPlayer){
                    willBeRemoved[i] = -1;
                }
            }

            willBeRemoved = sortArray(willBeRemoved);
            iWillBeRemoved--;

            for(int i = 0; i < iWillBeRemoved; i++){
                GameObject temp = handler.listBubble.get(willBeRemoved[i]);
                temp.isRemoved = true;
                handler.removeObject(temp);
            }
        }
        HUD.isFlying = false;
        if(Process.numberCollision >= 6){
            Process.numberCollision = 0;
            Process.addOneRow(handler);
        }
    }

    public static void findFloatingBubbles(Handler handler){

        for(int i = 0; i < iSolid; i++){

            if(solidBubble[i] == -1) break;
            if(contains(willBeRemoved, solidBubble[i])) continue;

            GameObject temp1 = handler.listBubble.get(solidBubble[i]);

            for(int j = 0; j < handler.listBubble.size(); j++){

                if(solidBubble[i] == j) continue;
                if(contains(willBeRemoved, j)) continue;

                GameObject temp2 = handler.listBubble.get(j);

                if(temp2.id == ID.Player) {
                    indexPlayer = j;
                    continue;
                }

                if((Hex.createRect(temp1.vertex).getBounds().intersects(Hex.createRect(temp2.vertex).getBounds()))  ){
                    temp2.isSolid = true;
                    if(!contains(solidBubble, j)){
                        solidBubble[iSolid] = j;
                        iSolid++;
                    }
                }
            }
        }
    }

    public static int[] sortArray(int[] arr){
        for(int k = 0; k < arr.length-1; k++){
            for(int l = k+1; l < arr.length; l++){
                if(arr[l] > arr[k]){
                    int temp = arr[l];
                    arr[l] = arr[k];
                    arr[k] = temp;
                }
            }
        }

        return arr;
    }

    public static boolean contains(int[] array, int m) {
        boolean contains = IntStream.of(array).anyMatch(x -> x == m);
        return contains;
    }

    public static BubbleInGrid addOneBubble(Handler handler, Point p){
        int color = random.nextInt(5)+1;
        BubbleInGrid nb = new BubbleInGrid(p, color, ID.Normal);
        return nb;
    }


    public static void addOneRow(Handler handler){
        Point p = Hex.lastBubble;

        if(Hex.lastBubble.x == (Hex.bordersX + Hex.r)){
            p.x += Hex.r;
        }
        else p.x -= Hex.r;
        p.y -= (Hex.s + Hex.h);
        for(int i = 0; i < 6; i++){
            handler.addObject(addOneBubble(handler, new Point(p.x + i*(Hex.a), p.y)));
        }

        Hex.lastBubble = new Point(p.x , p.y);
    }



}
