package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BubbleShooter extends Canvas implements Runnable {



    public static final int WIDTH = 480, HEIGHT = 720;
    private Thread thread;
    private boolean running = false;

    BufferedImage background = ImageIO.read(new File("res/bg1.png"));
    BufferedImage top = ImageIO.read(new File("res/topBg.png"));

    private Handler handler;
    private HUD hud;
    private int sh;
    private int countCurX;


    public BubbleShooter() throws IOException {
        sh = Hex.s + Hex.h;
        countCurX = Hex.bordersX;
        handler = new Handler();
        hud = new HUD(handler);
        JFrame frame = new Frame(WIDTH, HEIGHT, "Dynamite !", this);


        Process.addOneRow(handler);
        Process.addOneRow(handler);
        Process.addOneRow(handler);
        Process.addOneRow(handler);
        Process.addOneRow(handler);
        Process.addOneRow(handler);

        hud.newBubble();


        MouseInput ml = new MouseInput(handler, hud);
        addMouseListener(ml);
    }

    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized void stop(){
        try{
            thread.join();
            running = false;
        } catch(InterruptedException e){
            e.printStackTrace();
        }

    }


    @Override
    public void run() {
        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 10.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;

        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            while(delta >= 1){
                update();
                Hex.viewCurXCurY();
                delta--;
            }

            if(running){
                render();
            }
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                System.out.println("FPS: "+ frames);
                frames = 0;
            }
        }
        stop();
    }

    private void update(){
        handler.update();
    }

    private void render(){
        BufferStrategy bs = this.getBufferStrategy();
        if(bs == null){
            this.createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();
        g.drawImage(background, 0, 0, this);

        handler.render(g);

        //g.drawImage(top, 0, 0, this); //Create an image to hide some part of bubbleInGrid grid.

        g.dispose();
        bs.show();

    }

    public static void main(String[] args) throws IOException {

        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                Hex.setBORDERS(15);
                Hex.setSize(60);
                try {
                    new BubbleShooter();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

}
