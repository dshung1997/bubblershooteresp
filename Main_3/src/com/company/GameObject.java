package com.company;

import javax.swing.*;
import java.awt.*;

/**
 * Created by dshunggggg on 30/09/2016.
 */
public abstract class GameObject {
    protected int pos;
    protected int x;
    protected int y;
    protected int color;
    protected int velX, velY;

    protected Point vertex;
    protected Point center;
    protected ID id;
    public boolean isRemoved;
    public boolean isSolid;

    protected int pox, poy;

    protected Polygon polygonBubble;

    public GameObject(int pos, int color, ID id){
        this.pos = pos;
        this.x = pos % 6;
        this.y = pos / 6;
        this.id = id;
        this.color = color;

        vertex = Hex.getVertexFromGrid(true, y, x);
        center = Hex.getCenterFromVertex(vertex);
        isRemoved = false;
        polygonBubble = Hex.creatHex(vertex);
    }

    public GameObject(Point p, int color, ID id){
        this.color = color;
        this.vertex = p;
        this.id = id;
        center = Hex.getCenterFromVertex(vertex);
        pox = center.x; poy = center.y;
        polygonBubble = Hex.creatHex(vertex);
    }

    public abstract void update();

    public abstract void render(Graphics2D g2d);

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getVelY() {
        return velY;
    }

    public void setVelY(int velY) {
        this.velY = velY;
    }

    public int getVelX() {
        return velX;
    }

    public void setVelX(int velX) {
        this.velX = velX;
    }

    public Point getVertex() {
        return vertex;
    }

    public void setVertex(Point vertex) {
        this.vertex = vertex;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }
}
